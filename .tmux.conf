# Binding
unbind C-b
set -g prefix C-x
setw -g mouse on
bind v split-window -h
bind o split-window -v
unbind '"'
unbind %

set-option -g set-titles on
set-option -g set-titles-string '[#S:#I.#P] #W'
set-window-option -g automatic-rename on
set -g default-terminal "xterm-256color"

# messages:
set-window-option -g mode-bg "#303030"
set-window-option -g mode-fg white
set-option -g message-bg brightblack
set-option -g message-fg white

# panes:
set-option -g pane-border-fg white
set-option -g pane-border-bg default
set-option -g pane-active-border-fg blue
set-option -g pane-active-border-bg default

# status bar:
set-option -g status-justify right
set-option -g status-bg black
set-option -g visual-activity on
set-option -g status-fg white
set-option -g status-bg default
set-option -g status-attr default
set-window-option -g window-status-fg "#666666"
set-window-option -g window-status-bg default
set-window-option -g window-status-attr default
set-window-option -g window-status-current-fg "#cccccc" 
set-window-option -g window-status-current-bg "#262626"
set-window-option -g window-status-current-attr default
set-option -g message-fg white
set-option -g message-bg black
set-option -g message-attr bright
set -g status-left " "
set -g status-justify left
setw -g window-status-format         ' #(echo "#{pane_current_command}") '
setw -g window-status-current-format ' #(echo "#{pane_current_command}") '
set -g status-right " "

# vim:
is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
bind-key -n C-h if-shell "$is_vim" "send-keys C-h"  "select-pane -L"
bind-key -n C-j if-shell "$is_vim" "send-keys C-j"  "select-pane -D"
bind-key -n C-k if-shell "$is_vim" "send-keys C-k"  "select-pane -U"
bind-key -n C-l if-shell "$is_vim" "send-keys C-l"  "select-pane -R"
bind-key -n C-\ if-shell "$is_vim" "send-keys C-\\" "select-pane -l"
bind-key -T copy-mode-vi C-h select-pane -L
bind-key -T copy-mode-vi C-j select-pane -D
bind-key -T copy-mode-vi C-k select-pane -U
bind-key -T copy-mode-vi C-l select-pane -R
bind-key -T copy-mode-vi C-\ select-pane -l

# clock:
set-window-option -g clock-mode-colour white
set-window-option -g clock-mode-style 24
set -s escape-time 0
